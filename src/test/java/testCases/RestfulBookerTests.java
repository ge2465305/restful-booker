package testCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RestfulBookerTests {
	
	@Test
	public void test() {
		
		/**
		 * Test method for the Restful-booker API
		 * 
		 * First, a booking is created, then it's retrieved, updated and in the end it's deleted.
		 */
		try {
			RestfulMethods m = new RestfulMethods();
			
			/*Creating a booking*/
			String bookingID = m.createBooking("Agota", "Horvath", 600, true, "2023-05-09", "2023-05-22", "Breakfast");
			
			/*Retrieving booking data*/
			m.getBooking(bookingID);
			
			/*Updating booking*/
			m.updateBooking(bookingID, "Agota", "Horvath", 456, false, "2023-02-07", "2023-04-06", "Gym");
			
			/*Deleting booking*/
			m.deleteBooking(bookingID);
			
			/*Verifying that the booking was deleted. This method should throw an exception*/
			try {
				m.getBooking(bookingID);
			}
			catch(AssertionError e) {
				System.out.println("Booking with id " + bookingID + " was successfully deleted.");
			}
		}
		
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		
	}
}
