package testCases;

import org.apache.http.HttpStatus;
import org.hamcrest.core.Is;
import org.testng.Assert;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class RestfulMethods {

	public HashMap<String, Object> createHashMap(String firstname, String lastname, Integer totalprice, boolean depositpaid, String checkin, String checkout, String additionalneeds) {
		
		/**
		 * Creates a HashMap object for the booking creation and update
		 * 
		 * @param firstname first name of the person
		 * @param lastname last name of the person
		 * @param totalprice price of the booking
		 * @param depositpaid indicates whether the deposid was paid or not
		 * @param checkin date of the checkin
		 * @param checkout date of the checkout
		 * @param additionalneeds any additional needs
		 * @return HashMap filled with the data
		 */
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date checkinDate = null;
		Date checkoutDate = null;
		
		try {
			checkinDate = formatter.parse(checkin);
			checkoutDate = formatter.parse(checkout);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		HashMap<String, Date> bookingdates = new HashMap();
		bookingdates.put("checkin", checkinDate);
		bookingdates.put("checkout", checkoutDate);
		
		HashMap<String, Object> data = new HashMap();
		data.put("firstname", firstname);
		data.put("lastname", lastname);
		data.put("totalprice", totalprice);
		data.put("depositpaid", depositpaid);
		data.put("bookingdates", bookingdates);
		data.put("additionalneeds", additionalneeds);
		
		return data;
	}
	
	public String createToken() {
		
		/**
		 * Creates the token for authentication
		 */
		
		HashMap<String, String> data = new HashMap();
		data.put("username", "admin");
		data.put("password", "password123");
		
		Response response = 
			given()
				.contentType("application/json")
				.body(data)
			.when()
				.post("https://restful-booker.herokuapp.com/auth")
			.then()
				.statusCode(200)
				.extract().response();
		
		return response.body().jsonPath().getString("token");
	}
	
	public String createBooking(String firstname, String lastname, Integer totalprice, boolean depositpaid, String checkin, String checkout, String additionalneeds) {
		
		/**
		 * Creates a booking with the given data
		 * 
		 * @param firstname first name of the person
		 * @param lastname last name of the person
		 * @param totalprice price of the booking
		 * @param depositpaid indicates whether the deposid was paid or not
		 * @param checkin date of the checkin
		 * @param checkout date of the checkout
		 * @param additionalneeds any additional needs
		 * @return ID of the created booking
		 */
		
		Response response = 
			given()
				.contentType("application/json")
				.accept("application/json")
				.body(createHashMap(firstname, lastname, totalprice, depositpaid, checkin, checkout, additionalneeds))
			.when()
				.post("https://restful-booker.herokuapp.com/booking")
			.then()
				.assertThat()
				.statusCode(200)
				.body("booking.firstname", Is.is(firstname))
				.body("booking.lastname", Is.is(lastname))
				.body("booking.totalprice", Is.is(totalprice))
				.body("booking.depositpaid", Is.is(depositpaid))
				.body("booking.bookingdates.checkin", Is.is(checkin))
				.body("booking.bookingdates.checkout", Is.is(checkout))
				.body("booking.additionalneeds", Is.is(additionalneeds))
				.extract().response();
		
		String bookingID = response.jsonPath().get("bookingid").toString();
		
		System.out.println("\nBooking #" + bookingID + " was successfully created with the following data:\n");
		response.prettyPrint();
		
		return bookingID;
	}
	
	public void getBooking(String id) {
		
		/**
		 * Gets the data of the booking with the given ID
		 * 
		 * @param id ID of the booking that is to be retrieved
		 */
		
		Response response = 
				given()
					.accept("application/json")
				.when()
					.get("https://restful-booker.herokuapp.com/booking/" + id)
				.then()
					.assertThat()
					.statusCode(200)
					.extract().response();
		
		System.out.println("\nRetrieved data of booking #" + id + ":\n");
		response.prettyPrint();
	}
	
	public void updateBooking(String id, String firstname, String lastname, Integer totalprice, boolean depositpaid, String checkin, String checkout, String additionalneeds) {
		
		/**
		 * Updates a booking with the given data
		 * 
		 * @param id ID of the booking to be updated
		 * @param firstname first name of the person
		 * @param lastname last name of the person
		 * @param totalprice price of the booking
		 * @param depositpaid indicates whether the deposid was paid or not
		 * @param checkin date of the checkin
		 * @param checkout date of the checkout
		 * @param additionalneeds any additional needs
		 */
		
		Response response = 
				given()
					.header("Cookie", "token=" + createToken())
					.contentType("application/json")
					.accept("application/json")
					.body(createHashMap(firstname, lastname, totalprice, depositpaid, checkin, checkout, additionalneeds))
				.when()
					.put("https://restful-booker.herokuapp.com/booking/" + id)
				.then()
					.assertThat()
					.statusCode(200)
					.body("firstname", Is.is(firstname))
					.body("lastname", Is.is(lastname))
					.body("totalprice", Is.is(totalprice))
					.body("depositpaid", Is.is(depositpaid))
					.body("bookingdates.checkin", Is.is(checkin))
					.body("bookingdates.checkout", Is.is(checkout))
					.body("additionalneeds", Is.is(additionalneeds))
					.extract().response();
		
		System.out.println("\nUpdated data of booking #" + id + ":\n");
		response.prettyPrint();
	}
		
	
	public void deleteBooking(String id) {
		
		/**
		 * Deletes a booking
		 * 
		 * @param id ID of the booking to be deleted
		 */
		
		Response response = 
				given()
					.header("Cookie", "token=" + createToken())
					.contentType("application/json")
				.when()
					.delete("https://restful-booker.herokuapp.com/booking/" + id)
				.then()
					.assertThat()
					.statusCode(201)
					.extract().response();
		
		System.out.println("\n" + response.asString());
		System.out.println("Booking #" + id + " was successfully deleted.");
	}
	
}

